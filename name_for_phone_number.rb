require 'date'
class NameForPhoneNumber

  attr_accessor :phone_number, :result, :errors, :valid, :dictionary_file_path, :dictionary

  NUMBER_MAP = {"2" => ["a", "b", "c"], "3" => ["d", "e", "f"], "4" => ["g", "h", "i"], "5" => ["j", "k", "l"],
                "6" => ["m", "n", "o"], "7" => ["p", "q", "r", "s"], "8" => ["t", "u", "v"], "9" => ["w", "x", "y", "z"]}
  PHONE_NUMBER_COUNT = 10

  def initialize(phone_number, dictionary_file_path)
    @time_start = Time.now()
    @phone_number = phone_number
    @result = []
    @errors = []
    @valid = true
    @dictionary_file_path = dictionary_file_path
    @dictionary = {}
    valid?
  end

  def valid?
    if phone_number.nil? || phone_number.length != PHONE_NUMBER_COUNT || phone_number.match(/^[23456789]+$/).nil?
      self.errors << "Phone number should be #{PHONE_NUMBER_COUNT} digit and number should not include 0 and 1."
      self.valid = false
    else
      begin
        File.foreach( dictionary_file_path ) do |word|
          word = word.gsub(/\s+/, "")
          self.dictionary[word.length] = [] unless dictionary[word.length]
          self.dictionary[word.length] << word.chop.to_s.downcase
        end
      rescue
        self.errors << "File not exists or not readable."
        self.valid = false
      end
    end
  end

  def generate_the_friendly_name
    char_arr = phone_number.chars.map{|num| NUMBER_MAP[num]}

    # loop all the characters by combining minimum 2 characters
    for i in (1..PHONE_NUMBER_COUNT - 2)

      # first set of character array from 0 position to i
      first_set_num_chars = char_arr[0..i]
      next if first_set_num_chars.length < 2

      # second set of character array from i+1 position to PHONE_NUMBER_COUNT
      second_set_num_chars = char_arr[i + 1..PHONE_NUMBER_COUNT - 1]
      next if second_set_num_chars.length < 2

      # get combinations of words and match with the dictionary
      first_combinations = get_combination_of_chars(first_set_num_chars)
      next if first_combinations.nil?
      first_combinations = (first_combinations & dictionary[i+2])

      # get combinations of words and match with the dictionary
      second_combinations = get_combination_of_chars(second_set_num_chars)
      next if second_combinations.nil?
      second_combinations = second_combinations & dictionary[PHONE_NUMBER_COUNT - 1 - i + 1]

      # combin the first set and second set
      next if first_combinations.nil? || second_combinations.nil?
      first_combinations.product(second_combinations).each do |words|
        self.result << words
      end
    end

    # first word + second word = dictionary word
    self.result << (char_arr.shift.product(*char_arr).map(&:join) & dictionary[PHONE_NUMBER_COUNT]).join(", ")
  end

  def show_result
    generate_the_friendly_name
    puts "Time #{Time.now().to_f - @time_start.to_f}"
    puts result.inspect
  end

  def get_combination_of_chars(set)
    set.shift.product(*set).map(&:join)
  end

end


name_for_phone_number = NameForPhoneNumber.new("6686787825", "dictionary.txt")
if name_for_phone_number.valid
  name_for_phone_number.show_result
else
  puts name_for_phone_number.errors.join(", ")
end
